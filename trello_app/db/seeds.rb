# frozen_string_literal: true
seeder_names = %w[
  users
  cards
]

seeder_names.each do |seeder_name|
  path = Rails.root.join('db', 'seeds', "#{seeder_name}.rb")
  require(path)
end