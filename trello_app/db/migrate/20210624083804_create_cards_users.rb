class CreateCardsUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :cards_users do |t|
      t.bigint :user_id, null:false
      t.bigint :card_id, null:false

      t.timestamps
    end
    add_foreign_key :cards_users, :users, on_delete: :cascade
    add_foreign_key :cards_users, :cards, on_delete: :cascade
  end
end
