class CreateUserCards < ActiveRecord::Migration[6.1]
  def change
    create_table :user_cards do |t|
      t.bigint :user_id, null:false
      t.bigint :card_id, null:false

      t.timestamps
    end
    add_foreign_key :user_cards, :users, on_delete: :cascade
    add_foreign_key :user_cards, :cards, on_delete: :cascade
  end
end
