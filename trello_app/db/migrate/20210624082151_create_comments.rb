class CreateComments < ActiveRecord::Migration[6.1]
  def change
    create_table :comments do |t|
      t.bigint :user_id, null:false
      t.bigint :card_id, null:false
      t.text :content
      t.boolean :edit

      t.timestamps
    end
    add_foreign_key :comments, :users, on_delete: :cascade
    add_foreign_key :comments, :cards, on_delete: :cascade
  end
end
