# frozen_string_literal: true

Card.create(title: 'card 1',
            description: 'test card 1',
            status: '1',
            creator_id: 1
)
