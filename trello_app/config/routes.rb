Rails.application.routes.draw do
  namespace :v1 do
    post :'/users/login', to:'users#login'
    resources :checklists
    resources :checklist_parents
    resources :upload_files
    resources :actions
    resources :comments
    resources :users
    resources :cards
    post :'/cards/:id/assign-to-user', to:'cards#assignToUser'
    get :'/cards/:id/show-detail', to:'cards#showDetail'
    post :'/cards/move-card', to:'cards#moveCard'
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
