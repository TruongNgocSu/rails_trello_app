module CustomRenderResponse
  extend ActiveSupport::Concern

  def render_serialized_json_response(object, serializer, options = {})
    render_args = { json: object, root: :data, adapter: :json, status: 200, meta: { success: true }}
    render_args[:meta] = render_args[:meta].deep_merge(options) unless options.empty?
    if object.respond_to?(:ids) || object.is_a?(Array)
      render_args[:each_serializer] = serializer
    else
      render_args[:serializer] = serializer
    end
    render render_args
  end

  def render_invalid_json_response(errors, status: 422)
    render json: { success: false, message: :Validation_failed, errors: errors }, status: status
  end

  def render_author_json_response
    render json: { success: false, message: :Unauthorized }, status: 401
  end
end
