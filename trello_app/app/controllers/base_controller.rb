class BaseController < ApplicationController

  before_action :authenticated?

  include CustomRenderResponse

  def currentUser
    @user_id ||= User::Authenticate.run(token: token).result[0]['user_id'] if token
    @user ||= User.find(@user_id) unless @user_id.nil?
  end

  def token
    pattern = /^Bearer /
    header = request.headers['Authorization']
    header.gsub(pattern, '') if header && header.match(pattern)
  end

  def authenticated?
    render_author_json_response if currentUser.nil?
  end
end