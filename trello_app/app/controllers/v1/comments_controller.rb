class V1::CommentsController < BaseController
  # GET /comments
  def index
    @comments = Comment.all
    render_serialized_json_response(@comments, CommentSerializer)
  end

  # POST /comments
  def create
    outcome = Comment::CommentCreator.run(comment_params)
    return render_invalid_json_response(outcome.errors) unless outcome.valid?
    render_serialized_json_response(outcome.result, CommentSerializer)
  end

  # PATCH/PUT /comments/1
  def update
    outcome = Comment::CommentUpdater.run(comment_params.merge(id: params['id']))
    return render_invalid_json_response(outcome.errors) unless outcome.valid?
    render_serialized_json_response(outcome.result, CommentSerializer)
  end

  # DELETE /comments/1
  def destroy
    outcome = Comment::CommentDestroyer.run(id: params['id'])
    return render_invalid_json_response(outcome.errors) unless outcome.valid?
    render_serialized_json_response(outcome.result, CommentSerializer)
  end

  private

  # Only allow a list of trusted parameters through.
  def comment_params
    params.require(:comment).permit(:user_id, :card_id, :content)
  end
end
