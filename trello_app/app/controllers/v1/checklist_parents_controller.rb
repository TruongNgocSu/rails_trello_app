class V1::ChecklistParentsController < BaseController

  # GET /checklist_parents
  def index
    @checklist_parents = ChecklistParent.all

    render_serialized_json_response(@checklist_parents, ChecklistParentSerializer)
  end

  # POST /checklist_parents
  def create
    outcome = ChecklistParent::ChecklistParentCreator.run(checklist_parent_params)
    return render_invalid_json_response(outcome.errors) unless outcome.valid?
    render_serialized_json_response(outcome.result, ChecklistParentSerializer)
  end

  # PATCH/PUT /checklist_parents/1
  def update
    outcome = ChecklistParent::ChecklistParentUpdater.run(checklist_parent_params.merge(id: params['id']))
    return render_invalid_json_response(outcome.errors) unless outcome.valid?
    render_serialized_json_response(outcome.result, ChecklistParentSerializer)
  end

  # DELETE /checklist_parents/1
  def destroy
    outcome = ChecklistParent::ChecklistParentDestroyer.run(id: params['id'])
    return render_invalid_json_response(outcome.errors) unless outcome.valid?
    render_serialized_json_response(outcome.result, ChecklistParentSerializer)
  end

  private

    # Only allow a list of trusted parameters through.
    def checklist_parent_params
      params.require(:checklist_parent).permit(:card_id, :title)
    end
end
