class V1::ChecklistsController < BaseController

  # GET /checklists
  def index
    @checklists = Checklist.all
    render_serialized_json_response(@checklists, ChecklistSerializer)
  end

  # POST /checklists
  def create
    outcome = Checklist::ChecklistCreator.run(checklist_params)
    return render_invalid_json_response(outcome.errors) unless outcome.valid?
    render_serialized_json_response(outcome.result, ChecklistSerializer)
  end

  # PATCH/PUT /checklists/1
  def update
    outcome = Checklist::ChecklistUpdater.run(checklist_params.merge(id: params['id'], user_id: currentUser.id))
    return render_invalid_json_response(outcome.errors) unless outcome.valid?
    render_serialized_json_response(outcome.result, nil)
  end

  # DELETE /checklists/1
  def destroy
    outcome = Checklist::ChecklistDestroyer.run(id: params['id'])
    return render_invalid_json_response(outcome.errors) unless outcome.valid?
    render_serialized_json_response(outcome.result, ChecklistSerializer)
  end

  private

  # Only allow a list of trusted parameters through.
  def checklist_params
    params.require(:checklist).permit(:checklist_parent_id, :content, :status)
  end
end
