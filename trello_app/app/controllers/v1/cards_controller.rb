class V1::CardsController < BaseController

  # GET /cards
  def index
    @cards = Card.all

    render_serialized_json_response(@cards, CardSerializer)
  end

  # POST /cards
  def create
    outcome = Card::CardCreator.run(card_params)
    return render_invalid_json_response(outcome.errors) unless outcome.valid?
    render_serialized_json_response(outcome.result, CardSerializer)
  end

  # PATCH/PUT /cards/1
  def update
    outcome = Card::CardUpdater.run(card_params.merge(id: params['id']))
    return render_invalid_json_response(outcome.errors) unless outcome.valid?
    render_serialized_json_response(outcome.result, CardSerializer)
  end

  # DELETE /cards/1
  def destroy
    outcome = Card::CardDestroyer.run(id: params['id'])
    return render_invalid_json_response(outcome.errors) unless outcome.valid?
    render_serialized_json_response(outcome.result, CardSerializer)
  end

  #assign to user
  def assignToUser
    outcome = Card::CardAssign.run(cardAssign_params.merge(card_id: params['id'], user_id: currentUser.id))
    return render_invalid_json_response(outcome.errors) unless outcome.valid?
    render_serialized_json_response(outcome.result, nil)
  end

  #show detail
  def showDetail
    outcome = Card::ShowDetail.run(id: params['id'])
    return render_invalid_json_response(outcome.errors) unless outcome.valid?
    render_serialized_json_response(outcome.result, nil)
  end

  #move card
  def moveCard
    outcome = Card::MoveCard.run(cardList: params['cardList'])
    return render_invalid_json_response(outcome.errors) unless outcome.valid?
    render_serialized_json_response(outcome.result, nil)
  end

  private

  # Only allow a list of trusted parameters through.
  def card_params
    params.require(:card).permit(:title, :status, :description, :position, :creator_id)
  end

  def cardAssign_params
    params.require(:data).permit(:targetUser)
  end
end
