class V1::UsersController < BaseController
  before_action :set_user, only: [:show, :update, :destroy]
  skip_before_action :authenticated?, only: [:login]
  #login
  def login
    outCome = User::Login.run(login_params)
    return render_invalid_json_response(outCome.errors) unless outCome.valid?
    token = User::Token.run(user: outCome.result).result
    return render_serialized_json_response(outCome.result, UserSerializer, token: token)
  end

  # GET /users
  def index
    @users = User.all
    render_serialized_json_response(@users, UserSerializer)
  end

  # GET /users/1
  def show
    render json: @user
  end

  # POST /users
  def create
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def user_params
    params.require(:user).permit(:name, :email, :password)
  end

  def login_params
    params.require(:data).permit(:email, :password)
  end
end
