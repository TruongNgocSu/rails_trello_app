class CommentSerializer < ActiveModel::Serializer
  attributes :id,
             :card_id,
             :user_id,
             :content,
             :edit
end
