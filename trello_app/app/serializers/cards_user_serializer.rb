class CardsUserSerializer < ActiveModel::Serializer
  attributes :id,
             :card_id,
             :user_id
end
