class CardSerializer < ActiveModel::Serializer
  attributes :id,
             :title,
             :status,
             :description,
             :position,
             :creator_id,
             :created_at,
             :updated_at
end
