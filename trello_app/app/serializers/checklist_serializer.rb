class ChecklistSerializer < ActiveModel::Serializer
  attributes :id,
             :content,
             :status,
             :created_at,
             :updated_at,
             :checklist_parent_id
end
