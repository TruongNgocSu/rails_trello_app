class ChecklistParentSerializer < ActiveModel::Serializer
  attributes :id,
             :card_id,
             :title,
             :created_at,
             :updated_at
end
