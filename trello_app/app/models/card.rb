class Card < ApplicationRecord
  has_many :comments
  has_many :actions
  has_many :checklist_parents
  has_many :upload_files
  has_and_belongs_to_many :users
end
