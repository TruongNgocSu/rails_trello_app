class Checklist::ChecklistCreator < ActiveInteraction::Base

  string :checklist_parent_id
  string :content
  boolean :status

  validates :checklist_parent_id, presence: true
  validates :content, presence: true
  validates :status, inclusion: [true, false]

  def execute
    checklist = Checklist.new(inputs)
    checklist.save ? checklist : errors.merge!(checklist.errors)

  end
end
