class Checklist::ChecklistDestroyer < ActiveInteraction::Base
  integer :id

  validates :id, presence: true

  def execute
    (checklist = Checklist.destroy(id)) ? checklist : errors.merge!(checklist.errors)
  end
end
