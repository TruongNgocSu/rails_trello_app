class Checklist::ChecklistUpdater < ActiveInteraction::Base
  integer :id
  string :content
  boolean :status
  integer :checklist_parent_id
  integer :user_id

  validates :content, presence: true
  validates :status, inclusion: [true, false]

  def execute
    outcome = nil
    if status != Checklist.find(id)
      card_id = ChecklistParent.find(checklist_parent_id).card_id
      actionName = status ? "checkChecklist" : "noChecklist"
      outcome = Action::ActionCreator.run(action: actionName, card_id: card_id, user_id: user_id, content: content)
      return errors.merge!(outcome.errors) unless outcome.valid?
    end
    (checklist = Checklist.update(id, inputs.except(:id, :user_id))) ? checklist : errors.merge!(checklist.errors)
    return { data: checklist, action: (outcome.nil? ? [] : outcome.result) }
  end
end
