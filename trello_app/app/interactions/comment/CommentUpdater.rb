class Comment::CommentUpdater < ActiveInteraction::Base

  integer :id
  integer :user_id
  integer :card_id
  string :content
  boolean :edit, default:true

  validates :user_id, presence: true
  validates :card_id, presence: true
  validates :content, presence: true

  def execute
    (comment = Comment.update(id, inputs.except(:id))) ? comment : errors.merge!(comment.errors)
  end
end
