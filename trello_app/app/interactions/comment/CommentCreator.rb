class Comment::CommentCreator < ActiveInteraction::Base

  integer :user_id
  integer :card_id
  string :content
  boolean :edit, default:false

  validates :user_id, presence: true
  validates :card_id, presence: true
  validates :content, presence: true

  def execute
    comment = Comment.new(inputs)
    comment.save ? comment : errors.merge!(comment.errors)
  end
end
