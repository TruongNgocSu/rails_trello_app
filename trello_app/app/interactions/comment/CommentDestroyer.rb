class Comment::CommentDestroyer < ActiveInteraction::Base
  integer :id

  validates :id, presence: true

  def execute
    (comment = Comment.destroy(id)) ? comment : errors.merge!(comment.errors)
  end
end
