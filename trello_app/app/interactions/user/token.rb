class User::Token < ActiveInteraction::Base
  object :user
  integer :exp, default: 30.days.to_i
  string :key, default: "-----BEGIN RSA PRIVATE KEY-----\nMIIBOwIBAAJBALbkpbDFbZ54bM5ybwwdCqsUHjxWQF4B0Q1sAOBFEYdpxZJZ8dAz\nycPzIgSlPc8yqjeqwJQtvCpktrntALpX1ksCAwEAAQJAYT0XyvBs48BrOSgmWm5m\naab8nF/PQSv+FgDCRnryYue3WZOpUqITB0w6ivC68G/+Mf6IXyE4ljqw2iIAdjyv\nYQIhAOE20o2bLPMtziEOdH0KGpN0gNYpe38jGyvGw7k5gZd9AiEAz+TWZRJpc9yX\n5dew3xcBtIhaTPFmVLgmfU7FwIWW32cCIQCvKK9LmUO1gouN5CsvUNtokbTeW/cD\n467vNjDlb1deFQIhAK55pZ1p2GrOpgTWArEYg+vZy79rkbBkZJkh9UFgXIDdAiBm\nRglcmt9cD2Vqg7xMr7cP3FJbSmJffSwYve1fazuZOw==\n-----END RSA PRIVATE KEY-----"

  def execute

    now_seconds = Time.now.to_i
    data = { iat: now_seconds,
             user_id: user.id,
             exp: now_seconds + exp }
    JWT.encode data, getKeyRSA, 'RS256'
  end

  def getKeyRSA()
    OpenSSL::PKey::RSA.new(key)
  end
end
