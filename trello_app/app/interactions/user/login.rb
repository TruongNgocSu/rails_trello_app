class User::Login < ActiveInteraction::Base
  string :email
  string :password

  validates :email, presence: true
  validates :password, presence: true

  def execute
    user = User.find_by(email: email)
    if (user.nil? || !user.authenticate(password))
      authentication_failed
      return
    end
    user
  end

  private

  def authentication_failed()
    errors.add(:login_failded, "login_failded")
  end
end