class User::Authenticate < ActiveInteraction::Base

  string :key, default: "-----BEGIN RSA PRIVATE KEY-----\nMIIBOwIBAAJBALbkpbDFbZ54bM5ybwwdCqsUHjxWQF4B0Q1sAOBFEYdpxZJZ8dAz\nycPzIgSlPc8yqjeqwJQtvCpktrntALpX1ksCAwEAAQJAYT0XyvBs48BrOSgmWm5m\naab8nF/PQSv+FgDCRnryYue3WZOpUqITB0w6ivC68G/+Mf6IXyE4ljqw2iIAdjyv\nYQIhAOE20o2bLPMtziEOdH0KGpN0gNYpe38jGyvGw7k5gZd9AiEAz+TWZRJpc9yX\n5dew3xcBtIhaTPFmVLgmfU7FwIWW32cCIQCvKK9LmUO1gouN5CsvUNtokbTeW/cD\n467vNjDlb1deFQIhAK55pZ1p2GrOpgTWArEYg+vZy79rkbBkZJkh9UFgXIDdAiBm\nRglcmt9cD2Vqg7xMr7cP3FJbSmJffSwYve1fazuZOw==\n-----END RSA PRIVATE KEY-----"
  string :token

  def execute
    return JWT.decode token, getKeyRSA, true, algorithm: 'RS256'
  rescue StandardError
    nil
  end

  def getKeyRSA
    OpenSSL::PKey::RSA.new(key)
  end

end
