class Card::CardDestroyer < ActiveInteraction::Base
  integer :id

  validates :id, presence: true

  def execute
    (card = Card.destroy(id)) ? card : errors.merge!(card.errors)
  end
end
