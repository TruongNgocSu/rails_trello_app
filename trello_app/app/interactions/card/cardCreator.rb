class Card::CardCreator < ActiveInteraction::Base

  string :title
  string :description
  integer :status
  integer :position
  integer :creator_id

  validates :title, presence: true
  validates :description, presence: true
  validates :status, presence: true
  validates :position, presence: true
  validates :creator_id, presence: true

  def execute
    card = Card.new(inputs)
    unless card.save
      return errors.merge!(card.errors)
    end
    actionName = %w[addCardBacklog addCardTodo addCardInprogress addCardDone][card.status]
    outcome = Action::ActionCreator.run(action: actionName, card_id: card.id, user_id: creator_id, content: card.title)
    return errors.merge!(outcome.errors) unless outcome.valid?
    card
  end
end
