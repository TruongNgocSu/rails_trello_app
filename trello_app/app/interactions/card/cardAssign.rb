class Card::CardAssign < ActiveInteraction::Base

  integer :user_id
  integer :card_id
  integer :targetUser

  validates :user_id, presence: true
  validates :card_id, presence: true

  def execute
    outcome = nil
    hasOne = Action.where(card_id: card_id, content: targetUser, user_id: user_id).first
    cardsUser = CardsUser.new(inputs.except(:targetUser))
    return errors.merge!(cardsUser.errors) unless cardsUser.save
    actionName = hasOne.nil? ? "assignToUser" : "removeFormUser"
    outcome = Action::ActionCreator.run(action: actionName, card_id: card_id, user_id: user_id, content: targetUser.to_s)
    return errors.merge!(outcome.errors) unless outcome.valid?
    return { cardsUser: cardsUser, action: (outcome.nil? ? [] : outcome.result) }
  end
end
