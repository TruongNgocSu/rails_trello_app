class Card::CardUpdater < ActiveInteraction::Base
  integer :id
  string :title
  string :description
  integer :status
  integer :position
  integer :creator_id

  validates :id, presence: true
  validates :title, presence: true
  validates :description, presence: true
  validates :status, presence: true
  validates :position, presence: true
  validates :creator_id, presence: true

  def execute
    (card = Card.update(id, inputs.except(:id))) ? card : errors.merge!(card.errors)
  end
end
