class Card::MoveCard < ActiveInteraction::Base

  array :cardList

  def execute
    ids = []
    cardList.each do |item|
      ids.push(item['id'])
    end
    cardListUpdate = Card.where('id', ids)
    cardListUpdate.each do |card|
      hasCard = cardList.select { |item| item['id'] == card.id }
      card.update('status': hasCard[0]['id'], 'position': hasCard[0]['position']) unless hasCard.empty?
    end
    cardListUpdate
  end
end
