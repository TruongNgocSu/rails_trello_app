class Card::ShowDetail < ActiveInteraction::Base

  integer :id

  validates :id, presence: true

  def execute
    Card.where(id: id).first.to_json(include: [:comments, :users, :checklist_parents, :actions])
  end
end
