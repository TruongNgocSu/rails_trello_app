class ChecklistParent::ChecklistParentUpdater < ActiveInteraction::Base

  integer :card_id
  integer :id
  string :title

  validates :card_id, presence: true
  validates :title, presence: true
  validates :id, presence: true

  def execute
    (checklistParent = ChecklistParent.update(id, inputs.except(:id))) ? checklistParent : errors.merge!(checklistParent.errors)
  end
end
