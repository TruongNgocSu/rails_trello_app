class ChecklistParent::ChecklistParentDestroyer < ActiveInteraction::Base
  integer :id

  validates :id, presence: true

  def execute
    (checklistParent = ChecklistParent.destroy(id)) ? checklistParent : errors.merge!(checklistParent.errors)
  end
end
