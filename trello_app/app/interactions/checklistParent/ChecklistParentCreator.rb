class ChecklistParent::ChecklistParentCreator < ActiveInteraction::Base

  string :card_id
  string :title

  validates :card_id, presence: true
  validates :title, presence: true

  def execute
    checklistParent = ChecklistParent.new(inputs)
    checklistParent.save ? checklistParent : errors.merge!(checklistParent.errors)
  end
end
