class Action::ActionCreator < ActiveInteraction::Base

  string :action
  integer :card_id
  integer :user_id
  string :content

  validates :action, presence: true
  validates :content, presence: true
  validates :card_id, presence: true
  validates :user_id, presence: true

  def execute
    action = Action.new(inputs)
    action.save ? action : errors.merge!(action.errors)
  end
end

